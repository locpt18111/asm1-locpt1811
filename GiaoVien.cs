﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManagementSystem
{
    public class GiaoVien : NguoiLaoDong
    {
        private double HeSoLuong { get; set; }

        public GiaoVien() { }

        public double getHeSoLuong()
        {
            return this.HeSoLuong;
        }
        public GiaoVien(string hoten, int namsinh, double luongcoban, double hesoluong)
            : base(hoten,namsinh,luongcoban)
        {
            this.HeSoLuong = hesoluong;
        }

        public void NhapThongTin(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        {
            base.NhapThongTin(hoTen, namSinh, luongCoBan);
            this.HeSoLuong = heSoLuong;
        }

        public override double TinhLuong()
        {
            return  base.getLuongCoBan()*HeSoLuong * 1.25;
        }

        public override void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine($"He so luong : { HeSoLuong}, Luong: {TinhLuong()}");
        }

        public double XuLy()
        {
            return HeSoLuong + 0.6;
        }
    }
}
