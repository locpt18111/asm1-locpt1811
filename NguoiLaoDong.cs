﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManagementSystem
{
    public class NguoiLaoDong
    {
        private string HoTen { get; set; }
        private int NamSinh { get; set; }
        private double LuongCoBan { get; set; }

        public double getLuongCoBan()
        {
            return this.LuongCoBan;
        }
        public string getHoTen()
        {
            return this.HoTen;
        }
        public int getNamSinh()
        {
            return this.NamSinh;
        }
        public NguoiLaoDong() { }   

        public NguoiLaoDong(string hoten, int namsinh,double luongcoban)
        {
            this.HoTen = hoten;
            this.NamSinh = namsinh;
            this.LuongCoBan = luongcoban;
        }

        public void NhapThongTin(string HoTen, int NamSinh, double LuongCoBan) 
        {
            this.HoTen = HoTen;
            this.NamSinh = NamSinh;
            this.LuongCoBan = LuongCoBan;
        }

        public virtual double TinhLuong()
        {
            return LuongCoBan;
        }

        public virtual void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan} ");
        }
    }
}
