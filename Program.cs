﻿using TeacherManagementSystem;
using ConsoleTables;
class Program
{
    List<GiaoVien> list = new List<GiaoVien>();
    string colorWarning = "\u001b[31m";
    string endColor = "\u001b[0m";
    public void addGiaoVien()
    {
        GiaoVien giaoVien = new GiaoVien();
        string HoTen;
        int NamSinh;
        double LuongCoBan;
        double HeSoLuong;
        do
        {
            Console.Write("Nhap Ho ten: ");
            HoTen = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(HoTen))
            {
                Console.WriteLine($"{colorWarning}Ho ten must not empty. Please try again.{endColor}");

            }
        } while (string.IsNullOrWhiteSpace(HoTen));

        bool isValidNamSinh;
        do
        {
            Console.Write("Nhap Nam sinh: ");
            isValidNamSinh = int.TryParse(Console.ReadLine(), out NamSinh);

            if (!isValidNamSinh || NamSinh < 1700 || NamSinh > DateTime.Now.Year)
            {
                Console.WriteLine($"{colorWarning}Nam sinh isn't valid, must between 1700 to now. Please try again.{endColor}");
                isValidNamSinh = false; 
            }
        } while (!isValidNamSinh);

        bool isValidLuongCoBan;
        do
        {
            Console.Write("Nhap Luong co ban: ");
            isValidLuongCoBan = double.TryParse(Console.ReadLine(), out LuongCoBan);

            if (!isValidLuongCoBan || LuongCoBan < 0)
            {
                Console.WriteLine($"{colorWarning}Luong co ban isn't valid. Please try again. {endColor}");
                isValidLuongCoBan = false; 
            }
        } while (!isValidLuongCoBan);

        bool isValidHeSoLuong;
        do
        {
            Console.Write("Nhap He so luong: ");
            isValidHeSoLuong = double.TryParse(Console.ReadLine(), out HeSoLuong);

            if (!isValidHeSoLuong || HeSoLuong < 0)
            {
                Console.WriteLine($"{colorWarning}He so luong isn't valid. Please try again.{endColor}");
                isValidHeSoLuong = false;
            }
        } while (!isValidHeSoLuong);
        Console.WriteLine();
        giaoVien.NhapThongTin(HoTen, NamSinh, LuongCoBan, HeSoLuong);
        list.Add(giaoVien);
        var table = new ConsoleTable("HoTen", "NamSinh", "LuongCoBan", "HeSoLuong", "Luong");
        table.AddRow(giaoVien.getHoTen(),
                        giaoVien.getNamSinh(),
                        giaoVien.getLuongCoBan(),
                        giaoVien.getHeSoLuong(),
                        giaoVien.TinhLuong()
                        );
        table.Write();

    }

    public void displayLowestGiaoVien()
    {
        Console.WriteLine(); 

        if (list.Count == 0)
        {
            Console.WriteLine("Please add Giao Vien to use this function");
        }
        else
        {
            var table = new ConsoleTable("HoTen", "NamSinh", "LuongCoBan", "HeSoLuong", "Luong");
            GiaoVien lowestSalaryGiaoVien = list[0];
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].TinhLuong() < lowestSalaryGiaoVien.TinhLuong())
                {
                    lowestSalaryGiaoVien = list[i];
                }
            }

            foreach (GiaoVien giaoVien in list)
            {
                if (lowestSalaryGiaoVien.TinhLuong() == giaoVien.TinhLuong())
                    table.AddRow(giaoVien.getHoTen(),
                        giaoVien.getNamSinh(),
                        giaoVien.getLuongCoBan(),
                        giaoVien.getHeSoLuong(),
                        giaoVien.TinhLuong()
                        );
            }
            table.Write();
            Console.WriteLine();
             Console.WriteLine("Press anything to continue");
            Console.ReadLine();
        }

    }

    public void displayAllGiaoVien()
    {
        Console.WriteLine();
        if (list.Count == 0)
        {
            Console.WriteLine("Please add Giao Vien to use this function");
        }
        else
        {
            var table = new ConsoleTable("HoTen", "NamSinh", "LuongCoBan", "HeSoLuong", "Luong");
            foreach (GiaoVien giaoVien in list)
            {
                table.AddRow(giaoVien.getHoTen(),
                        giaoVien.getNamSinh(),
                        giaoVien.getLuongCoBan(),
                        giaoVien.getHeSoLuong(),
                        giaoVien.TinhLuong()
                        );
            }
            table.Write();
            Console.WriteLine();
            Console.WriteLine("Press anything to continue");
            Console.ReadLine();
        }

    }
    static void Main(string[] args)
    {
        Program program = new Program();

        Boolean isOut = false;
        while(!isOut) 
        {
            int option;
            Console.WriteLine();
            Console.WriteLine("--------------------Menu------------------");
            Console.WriteLine("");
            Console.WriteLine("[1] Add new Giao Vien");
            Console.WriteLine("[2] Display Giao Vien with lowest salary");
            Console.WriteLine("[3] Display all Giao Vien");
            Console.WriteLine("[4] Exit");
            Console.WriteLine();
            Console.WriteLine("------------------------------------------");

            bool isValidOption;
            do
            {
                Console.Write("Please choose one option: ");
                isValidOption = int.TryParse(Console.ReadLine(), out option);

                if (!isValidOption || option < 1 || option > 4)
                {
                    Console.WriteLine($"\u001b[31mOption from 1 to 4. Please try again.\u001b[0m");
                    isValidOption = false;
                }
            } while (!isValidOption);

            switch (option)
            {
                case 1:
                    program.addGiaoVien();
                    break;
                case 2:
                    program.displayLowestGiaoVien();
                    break;
                case 3:
                    program.displayAllGiaoVien();
                    break;
                case 4:
                    Environment.Exit(0);
                    break;
            }
        }
     
        
    }
        
}